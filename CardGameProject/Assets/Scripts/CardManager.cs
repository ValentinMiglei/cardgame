﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CardInstance
{
    public string _name;
    public Sprite _logo;
    public int _attack, _defence;

    public CardInstance (string name, string logoPath, int attack, int defence)
    {
        _name = name;
        _logo = Resources.Load<Sprite>(logoPath);
        _attack = attack;
        _defence = defence;
    }
}

public static class CardManagerList
{
    public static List<CardInstance> AllCards = new List<CardInstance>();
}

public class CardManager : MonoBehaviour
{



}

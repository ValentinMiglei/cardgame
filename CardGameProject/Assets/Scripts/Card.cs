﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Card : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Camera _mainCamera;
    private Vector3 _offset;
    private GameObject _tempCardGameObject;
    public Transform _defaultParrent;
    public Transform _defaultTempCardParent;


    private void Awake()
    {
        _mainCamera = Camera.allCameras[0];
        _tempCardGameObject = GameObject.Find("TempCardGameObject");
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        _offset = transform.position - _mainCamera.ScreenToWorldPoint(eventData.position);

        _defaultParrent = _defaultTempCardParent = transform.parent;


        _tempCardGameObject.transform.SetParent(_defaultParrent);
        _tempCardGameObject.transform.SetSiblingIndex(transform.GetSiblingIndex());

        transform.SetParent(_defaultParrent.parent);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 newPosition = _mainCamera.ScreenToWorldPoint(eventData.position);

        transform.position = newPosition + _offset;

        if (_tempCardGameObject.transform.parent != _defaultTempCardParent)
            _tempCardGameObject.transform.SetParent(_defaultTempCardParent);

        CheckPosition();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(_defaultParrent);
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        transform.SetSiblingIndex(_tempCardGameObject.transform.GetSiblingIndex());
        _tempCardGameObject.transform.SetParent(GameObject.Find("Canvas").transform);
        _tempCardGameObject.transform.localPosition = new Vector3(2607, 0);
    }

    private void CheckPosition()
    {
        int newIndex = _defaultTempCardParent.childCount;

        for (int i = 0; i < _defaultTempCardParent.childCount; i++)
        {
            if (transform.position.x < _defaultTempCardParent.GetChild(i).position.x)
            {
                newIndex = i;

                if (_tempCardGameObject.transform.GetSiblingIndex() < newIndex)
                {
                    newIndex--;
                }
                break;
            }
        }

        _tempCardGameObject.transform.SetSiblingIndex(newIndex);

    }
}
